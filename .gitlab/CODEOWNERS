#####
# Quick intro to Codeowners
#
# The format is:
# [section]
# /<directory> <gitlab-user>
#
# The file is organized in to named sections denoting an internal organization function. e.g. People Group, Engineering, etc.
#
# ^[section] - Denotes an optional set of CodeOwners
#              No ^ means one of these Codeowners are required to approve MRs for this section
#
# For more information see https://docs.gitlab.com/ee/user/project/codeowners/
#
# Special notes:  Due to the current implementation in our code base sections can't have spaces in their names.
#                 Sections can't have named codeowners e.g. [section] @codeowner
#
#####

# Please make sure that @gitlab-com/handbook and  @gitlab-com/egroup groups are part of all
# entires where the codeowners are required.  Thats any entry where the group `[]` isn't prefixed
# with a Caret `^`.

# The following entries are all sections which require codeowner approval
# either because they relate to the function of the Handbook or because
# they are a controlled Document.

# The following entries relate to the operation of the handbook repo or the handbook site itself.
[handbook-operations]
/.editorconfig @gitlab-com/handbook
/.gitignore @gitlab-com/handbook
/.gitlab-ci.yml @gitlab-com/handbook
/.markdownlint-cli2.jsonc @gitlab-com/handbook
/.markdownlint.yaml @gitlab-com/handbook
/go.sum @gitlab-com/handbook
/go.mod @gitlab-com/handbook
/package-lock.json @gitlab-com/handbook
/package.json @gitlab-com/handbook
/.gitlab/ @gitlab-com/handbook
/config/ @gitlab-com/handbook
/layouts/ @gitlab-com/handbook
/scripts/ @gitlab-com/handbook

# handbook content
/content/_index.md @gitlab-com/handbook
/content/featured-background.png @gitlab-com/handbook
/content/handbook/about/ @gitlab-com/handbook
/content/handbook/content-websites/ @gitlab-com/handbook

# handbook docs
/LICENCE @gitlab-com/handbook
/README.md @gitlab-com/handbook
/CONTRIBUTING.md @gitlab-com/handbook
/content/docs/ @gitlab-com/handbook

# No point having a controlled documents section if anyone can just update
# The CODEOWNERS file.
[CODEOWNERS]
/.gitlab/CODEOWNERS @gitlab-com/egroup @gitlab-com/ceo-chief-of-staff-team @gitlab-com/handbook

# This is the controlled documents section where approval from a CODEOWNER is always required
# Please make sure that  @gitlab-com/handbook and  @gitlab-com/egroup groups are part of all
# controlled documents entries.
[Controlled-Documents]
/content/handbook/people-group/acceptable-use-policy.md @wendybarnes @akramer @emilyplotkin @pegan @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/access-management-policy.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/security-compliance/access-reviews.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-engineering/application-security/vulnerability-management.md @joshlemos @jritchey @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/audit-logging-policy.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/gitlab-business-continuity-plan.md @brobins @cmestel @NabithaRao @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/security-risk/storm-program/business-impact-analysis.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/controlled-document-procedure.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/cryptographic-standard.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/data-classification-standard.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/legal/privacy/dpia-policy.md @robin @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/data-team/data-management.md @amiebright @dvanrooijen2 @iweeks @nmcavinue @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/data-team/platform/_index.md @amiebright @dvanrooijen2 @iweeks @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/threat-management/vulnerability-management/encryption-policy.md @smanzuik @estrike @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/end-user-services/onboarding-access-requests/endpoint-management/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/security-compliance/security-control-lifecycle.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/password-standard.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/isms.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/end-user-services/self-help-troubleshooting/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/business-technology/end-user-services/onboarding-access-requests/ @rrea1 @pkaldis @NabithaRao @ericrubin @mbeee @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/people-group/offboarding/offboarding_standards.md @wendybarnes @mpatel8 @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/penetration-testing-policy.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/people-policies/ @cgudgenov @emilyplotkin @pegan @wendybarnes @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/records-retention-deletion.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/security-risk/storm-program/ @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-change-management-procedure.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/observation-management-procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/observation-remediation-procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-operations/sirt/security-incident-communication-plan.md @joshlemos @jfuentes2 @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-operations/sirt/sec-incident-response.md @joshlemos @jfuentes2 @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/secops-oncall.md @joshlemos @jlongo_gitlab @jfuentes2 @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/governance/sec-training.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/security-risk/third-party-risk-management.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/token-management-standard.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/threat-management/vulnerability-management/ @smanzuik @estrike @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/change-management-policy.md @joshlemos @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/finance/expenses.md @brobins @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/dedicated-compliance/poam-deviation-request-procedure.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/security/security-assurance/control-health-effectiveness-rating.md @jburrows001 @tdilbeck @corey-oas @lcoleman @jlongo_gitlab @gitlab-com/egroup @gitlab-com/handbook

# These optional entries are for when no codeowner is found.
^[content-sites]
* @gitlab-com/egroup @gitlab-com/handbook

^[job-families]
/content/job-families/ @sytses @streas @gitlab-com/egroup

^[handbook]
/content/handbook/ @sytses @streas @gitlab-com/egroup

^[Culture]
/content/handbook/company/culture/ @streas
/content/handbook/company/culture/all-remote/ @streas
/content/handbook/company/culture/contribute/ @akramer
/content/handbook/company/culture/cross-culture-collaboration-guide.md @glucchesi @wendybarnes
/content/handbook/company/culture/gitlab-101.md @chrisweber44 @streas
/content/handbook/company/culture/inclusion/ @wendybarnes
/content/handbook/company/culture/internal-feedback.md @wendybarnes @mwilkins
/content/handbook/company/culture/inclusion/tmrg-gitlab-generational-understanding.md @darwinjs @wendybarnes @mdrussell @terrichu
/content/handbook/company/culture/inclusion/tmrg-gitlab-gente.md @pmejia @romerg
/content/handbook/company/culture/inclusion/erg-gitlab-diversability.md @jgladen @sam.white @kchappell
/content/handbook/company/culture/inclusion/tmrg-global-voices.md @eliran.mesika @twoodham @marin

# The rest of the entries are for the different sections of the handbook and/or job-families
^[READMEs]
/content/handbook/ceo/chief-of-staff-team/readmes/dtuan.md @dtuan
/content/handbook/ceo/chief-of-staff-team/readmes/yyugitlab.md @yyugitlab
/content/handbook/ceo/chief-of-staff-team/readmes/dlangemak.md @dlangemak
/content/handbook/ceo/chief-of-staff-team/readmes/ipedowitz.md @ipedowitz
/content/handbook/ceo/chief-of-staff-team/readmes/jamiemaynard.md @jamiemaynard
/content/handbook/ceo/chief-of-staff-team/readmes/streas.md @streas

^[alliances]
/content/handbook/alliances/ @nbadiey
/content/job-families/alliances/ @nbadiey

^[ceo]
/content/job-families/chief-executive-officer @sytses @streas
/content/job-families/board-of-directors/ @sytses @robin
/content/handbook/leadership/ @sytses @streas
/content/handbook/company/strategy.md @sytses
/content/handbook/company/stewardship.md @sytses
/content/handbook/company/okrs/ @streas
/content/handbook/company/history.md @sytses
/content/handbook/company/quote-to-cash.md @justinfarris @ofernandez2 @tgolubeva @courtmeddaugh @alex_martin @vincywilson @jackib @chloeliu @jeromezng @jameslopez @csouthard @rhardarson @isandin @shreyasagarwal @jbrennan1 @jrabbits @NabithaRao @sbaranidharan @jesssalcido @iweeks @lwhelihan @s_mccauley @annapiaseczna @andrew_murray @msubramanian

^[chief-information-security-officer]
/content/job-families/chief-information-security-officer.md @robin @fofungwu

^[company]
/content/handbook/company/ @sytses @streas @gitlab-com/egroup
/content/handbook/esg.md @robin @slcline
/content/handbook/company/structure.md @wendybarnes @cbednarz @anjali_kaufmann @dparsonage @glucchesi @carlierussell @rtakken
/content/handbook/company/family-and-friends-day.md @chrisweber44

^[customer-success]
/content/handbook/customer-success @spatching @christiaanconover @mrleutz

^[eba-team]
/content/handbook/eba/ @chrisweber44

^[engineering]
/content/handbook/engineering/ @akramer
/content/job-families/engineering/ @joergheilig
/content/job-families/engineering/incubation/ @bmarnane

^[finance]
/content/handbook/finance/ @brobins
/content/handbook/finance/financial-planning-and-analysis/ @cmestel
/content/handbook/finance/financial-planning-and-analysis/sales-finance/ @jlatendresse @kmckern @ofalken
/content/handbook/finance/financial-planning-and-analysis/adaptive-insights.md @cmestel
/content/handbook/finance/financial-planning-and-analysis/sales-finance/gtm-analytics-hub.md @cmestel @jakebielecki @christinelee @alex.cohen @james_harrison @jbrennan1
/content/handbook/finance/procurement/ @cmestel @brobins
/content/handbook/finance/growth-and-development/cfo-shadow-program.md @brobins @dtadich
/content/handbook/finance/timekeeping/ @TaraKumpf @cgudgenov @simon-mundy
/content/handbook/spending-company-money.md @brobins @daleb04
/content/handbook/tax/ @jgladen
/content/job-families/finance/ @brobins

^[Infrastructure-Standards]
/content/handbook/infrastructure-standards/ @jeffersonmartin @dawsmith @alanrichards

^[legal]
/content/handbook/legal/gitlab-code-of-business-conduct-and-ethics.md @robin
/content/handbook/legal/ @robin
/content/handbook/legal/esg/ @robin @slcline @kbuncle
/content/handbook/legal/esg/esg-training.md @robin @slcline @kbuncle
/content/job-families/legal-and-corporate-affairs/ @robin

^[marketing]
/content/handbook/marketing/ @akramer
/content/handbook/marketing/marketing-strategy-and-platforms.md @akramer @christinelee @mpreuss22 @s_awezec @RLeihe268
/content/handbook/marketing/strategy-performance/allocadia/ @rcallam @emathis @lblanchard
/content/handbook/marketing/plan-fy22.md @lblanchard
/content/handbook/marketing/plan-fy23.md @lblanchard
/content/handbook/marketing/channel-marketing/ @cmaynard1 @lblanchard
/content/handbook/marketing/inbound-marketing/ @akramer @mpreuss22
/content/handbook/marketing/digital-experience/ @mpreuss22 @fqureshi @justin.vetter @laurenbarker
/content/handbook/marketing/growth.md @akramer @s_awezec @gdoud
/content/handbook/marketing/inbound-marketing/search-marketing/ @ncregan @hsmith-watson @akramer
/content/handbook/marketing/inbound-marketing/search-marketing/analytics.md @akramer
/content/handbook/marketing/inbound-marketing/search-marketing/testing.md @mpreuss22 @akramer
/content/handbook/marketing/inbound-marketing/search-marketing/seo-strategy.md @akramer @ncregan @hsmith-watson
/content/handbook/marketing/marketing-career-development/ @akramer
/content/handbook/marketing/demand-generation/ @akramer
/content/handbook/marketing/demand-generation/campaigns/ @aoetama @eirinipan @ikryzeviciene @srances @meilynda @ttran5
/content/handbook/marketing/campaigns/ @aoetama @eirinipan @ikryzeviciene @srances @meilynda @ttran5
/content/handbook/marketing/utm-strategy.md @ikryzeviciene @rkohnke @aoetama
/content/handbook/marketing/lifecycle-marketing/ @aklatzkin @dambrold
/content/handbook/marketing/lifecycle-marketing/emails-nurture/ @aklatzkin @dambrold
/content/handbook/marketing/lifecycle-marketing/agency-verticurl.md @dambrold @aklatzkin @krogel
/content/handbook/marketing/emergency-response.md @akramer @rkohnke @amy.waller
/content/handbook/marketing/events/ @akramer
/content/handbook/marketing/localization/ @djsulliv
/content/handbook/marketing/marketing-operations/ @christinelee @amy.waller
/content/handbook/marketing/sales-plays-cicd/ @dsteer @supadhyaya
/content/handbook/marketing/smb-marketing.md @ikryzeviciene @meilynda @bstallings @warias @supadhyaya @dsteer
/content/handbook/marketing/project-management-guidelines/ @johncoghlan @supadhyaya
/content/handbook/marketing/readmes/ @akramer @mpreuss22
/content/handbook/marketing/sales-development/ @akramer @jlarramendy
/content/handbook/marketing/team-member-social-media-policy.md @rchachra @robin @award4
/content/handbook/marketing/blog/ @dsteer @Sgittlen
/content/handbook/marketing/blog/release-posts/  @justinfarris @dsteer @Sgittlen
/content/handbook/marketing/account-based-marketing/ @megan_odowd @marcigee
/content/handbook/marketing/account-based-marketing/ideal-customer-profile.md @megan_odowd @marcigee
/content/handbook/marketing/account-based-marketing/key-account-lists.md @megan_odowd @marcigee @jcaine
/content/handbook/marketing/marketing-enablement.md  @justinfarris
/content/handbook/marketing/developer-relations/ @esalvadorp @akramer
/content/handbook/marketing/developer-relations/contributor-success/ @esalvadorp @nick_vh
/content/handbook/marketing/developer-relations/community-programs/education-program/ @esalvadorp @johncoghlan
/content/handbook/marketing/developer-relations/evangelist-program.md @esalvadorp @johncoghlan
/content/handbook/marketing/developer-relations/leading-organizations/ @nick_vh
/content/handbook/marketing/developer-relations/community-programs/ @esalvadorp
/content/handbook/marketing/developer-relations/community-programs/open-source-program/ @esalvadorp @bbehr
/content/handbook/marketing/developer-relations/community-programs/startups-program/ @esalvadorp
/content/handbook/marketing/developer-relations/strategic-plans/contributor-program-unification.md @nick_vh @johncoghlan
/content/handbook/marketing/developer-relations/program-resources.md @esalvadorp @johncoghlan
/content/handbook/marketing/developer-relations/project-management.md @esalvadorp @johncoghlan
/content/handbook/marketing/developer-relations/developer-evangelism/ @abuango @dnsmichi @johncoghlan @sugaroverflow @warias
/content/handbook/marketing/developer-relations/technical-marketing/ @johncoghlan @csaavedra1 @fjdiaz @iganbaruch
/content/handbook/marketing/developer-relations/workflows-tools/ @abuango @dnsmichi @johncoghlan @sugaroverflow @warias
/content/handbook/marketing/corporate-communications/ @akramer @cweaver1
/content/handbook/marketing/corporate-communications/resources-trainings.md @cweaver1 @award4
/content/handbook/marketing/corporate-communications/incident-communications-plan.md @robin @johncoghlan @award4
/content/job-families/marketing/ @akramer

^[Marketing-Integrated-Marketing]
/content/handbook/marketing/integrated-marketing/ @akramer @cmaynard1
/content/handbook/marketing/integrated-marketing/corporate-events.md @cmaynard1 @kdemarest
/content/handbook/marketing/integrated-marketing/digital-strategy/ @cmaynard1 @bstallings
/content/handbook/marketing/integrated-marketing/digital-strategy/social-marketing/ @award4
/content/handbook/marketing/integrated-marketing/digital-strategy/social-marketing/social-reporting.md @award4
/content/handbook/marketing/field-marketing/ @cmaynard1 @lblanchard

^[Marketing-Brand-and-Product-Marketing]
/content/handbook/marketing/brand-and-product-marketing/ @akramer @dsteer @rclayman
/content/handbook/marketing/brand-and-product-marketing/brand/ @akramer @dsteer @rclayman
/content/handbook/marketing/brand-and-product-marketing/brand/brand-activation/ @luke @amittner
/content/handbook/marketing/brand-and-product-marketing/brand/brand-activation/brand-standards.md @amittner @luke @monica_galletto @vicbell
/content/handbook/marketing/brand-and-product-marketing/design.md @akramer @dsteer @amittner @luke @monica_galletto @vicbell
/content/handbook/marketing/brand-and-product-marketing/content/ @akramer @dsteer
/content/handbook/marketing/brand-and-product-marketing/content/content-marketing.md @akramer @dsteer
/content/handbook/marketing/brand-and-product-marketing/content/editorial-team.md @akramer @dsteer
/content/handbook/marketing/brand-and-product-marketing/content/digital-production/ @amittner
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/ @dsteer @supadhyaya
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/market-insights.md @akramer @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/analyst-relations/ @rragozzine @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/competitive-intelligence/ @dsteer @jkempton
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/customer-advocacy/ @nicolecsmith @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/customer-advocacy/case-studies.md @nicolecsmith @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/demo/ @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/mrnci.md @dsteer
/content/handbook/use-cases.md @supadhyaya @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/ @supadhyaya @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/core-product-marketing/ @supadhyaya @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/public-sector-gtm.md @supadhyaya @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/version-control-collaboration/ @supadhyaya @warias @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/ci/ @iganbaruch @dsteer @supadhyaya
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/devsecops/ @supadhyaya @fjdiaz @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/devops-platform/ @warias @dsteer
/content/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/usecase-gtm/gitops/ @supadhyaya @csaavedra1 @dsteer

^[Netlify-CMS]
/content/handbook/marketing/netlifycms.md @laurenbarker @justin.vetter @mpreuss22

^[paid-time-off]
/content/handbook/paid-time-off.md @lyndemeiers

^[People]
/content/handbook/leadership/underperformance.md @wendybarnes @pegan @cbednarz @rtakken @glucchesi
/content/handbook/people-group/ @wendybarnes @pegan @cbednarz @rallen3 @mpatel8 @cgudgenov
/content/handbook/people-group/employment-solutions.md @pegan @hdevlin
/content/handbook/people-group/contracts-probation-periods.md @mpatel8 @shensiek @alex_venter @Mowry @nutaurus
/content/handbook/people-group/people-compliance/ @cgudgenov
/content/handbook/people-group/general-onboarding/ @mpatel8 @Mowry @alex_venter @anechan @ashjammers @nutaurus
/content/handbook/people-group/promotions-transfers.md @wendybarnes @mpatel8
/content/handbook/people-group/offboarding/ @wendybarnes @mpatel8
/content/handbook/people-group/relocation.md @wendybarnes @pegan @mpatel8
/content/handbook/people-group/employment-branding/ @drogozinski @cchiodo @rallen3
/content/handbook/people-group/employment-branding/people-communications.md @kaylagolden @drogozinski @rallen3
/content/handbook/group-conversations.md @chrisweber44
/content/handbook/people-group/celebrations.md @wendybarnes @mpatel8
/content/handbook/people-group/talent-assessment.md @wendybarnes @rtakken @glucchesi @pegan @cbednarz
/content/handbook/labor-and-employment-notices/ @pegan @wendybarnes @cgudgenov @streas @sytses
/content/handbook/people-policies/leave-of-absence/ @lyndemeiers @pegan @cgudgenov
/content/handbook/people-policies/leave-of-absence/us.md @lyndemeiers @pegan @cgudgenov
/content/handbook/people-group/team-member-relations.md @pegan @atisdale
/content/handbook/people-group/pronouns.md @klang @ahanselka @lmcnally1
/content/job-families/people-group/recruiting-operations-insights/ @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/candidate-experience.md @sytses @streas @wendybarnes @rallen3
/content/job-families/people-group/ @wendybarnes @pegan

^[People-Engineering]
/content/handbook/people-group/engineering/ @anechan

^[product]
/content/handbook/product/ @david
/content/job-families/product/technical-writer.md @susantacker @dianalogan @kpaizee
/content/job-families/product/product-design-management.md @vkarnes
/content/job-families/product/product-designer.md @vkarnes
/content/job-families/product/ux-fullstack-engineer.md @susantacker
/content/job-families/product/ux-research-manager.md @asmolinski2
/content/job-families/product/ux-research-operations-coordinator.md @asmolinski2
/content/job-families/product/ux-researcher.md @asmolinski2
/content/job-families/product/technical-writing-manager.md @susantacker @dianalogan @kpaizee
/content/job-families/product/pricing.md @justinfarris
/content/job-families/product/product-analyst.md @justinfarris @cbraza
/content/job-families/product/product-management-leadership.md @david @hbenson @mflouton @justinfarris
/content/job-families/product/product-manager.md @david @justinfarris @hbenson @mflouton @joshlambert @fzimmer @jreporter @ofernandez2
/content/job-families/product/ @david @vkarnes @mflouton @justinfarris @hbenson

^[sales]
/content/handbook/resellers/ @ecepulis
/content/handbook/resellers/training.md @chrisweber44  @ecepulis
/content/handbook/sales/ @chrisweber44  @jbrennan1 @jakebielecki @james_harrison @dhong
/content/handbook/sales/account-planning/ @lschoenfeld @RobbieB @chrisweber44  @emelier @kgavalas
/content/handbook/sales/assistance-from-investors.md @james_harrison
/content/handbook/sales/build-value-with-customers.md @chrisweber44
/content/handbook/sales/channel/ @ecepulis
/content/handbook/sales/club/ @kdemarest @kagarrett
/content/handbook/sales/command-of-the-message/ @chrisweber44  @supadhyaya @dsteer
/content/handbook/sales/commercial/ @hmason @jvpotter @mzimmers @chrisweber44 @kyla @steve_xu @Clitheroe @Lvasquez00887 @carolynharding
/content/handbook/sales/commissions/ @james_harrison
/content/handbook/sales/faq-from-prospects.md @chrisweber44
/content/handbook/sales/field-communications/ @monicaj @shannonthompson
/content/handbook/sales/field-manager-development.md @ashahrazad1
/content/handbook/sales/field-operations/ @chrisweber44  @jbrennan1 @jakebielecki @james_harrison
/content/handbook/sales/field-operations/channel-operations/ @nscala
/content/handbook/sales/field-operations/customer-success-operations/ @jakebielecki
/content/handbook/sales/field-operations/field-enablement/ @ashahrazad1 @kshirazi @monicaj
/content/handbook/sales/field-operations/gtm-resources/ @james_harrison
/content/handbook/sales/field-operations/order-processing.md @jrabbits
/content/handbook/sales/field-operations/release-schedule.md @monicaj
/content/handbook/sales/field-operations/sales-operations/ @james_harrison
/content/handbook/sales/field-operations/sales-operations/deal-desk.md @jrabbits
/content/handbook/sales/field-operations/sales-strategy/ @jakebielecki
/content/handbook/sales/field-operations/sales-systems/ @jbrennan1 @sheelaviswanathan
/content/handbook/sales/field-operations/sfdc.md @jbrennan1
/content/handbook/sales/forecasting.md @james_harrison
/content/handbook/sales/gainsight/ @jakebielecki
/content/handbook/sales/idea-to-production-demo/ @chrisweber44
/content/handbook/sales/insidesales.md @james_harrison
/content/handbook/sales/it-agility-director-interview.md @chrisweber44
/content/handbook/sales/manager-operating-rhythm/ @ashahrazad1 @chrisweber44
/content/handbook/sales/manager-operating-rhythm/enterprise-sales.md @emelier @kshirazi
/content/handbook/sales/meddppicc.md @ashahrazad1 @jblevins608
/content/handbook/sales/negotiate-to-close.md @emelier @kshirazi
/content/handbook/sales/onboarding/ @jblevins608 @ashahrazad1
/content/handbook/sales/playbook/ @emelier @kshirazi
/content/handbook/sales/prospecting.md @emelier @chrisweber44 @kshirazi
/content/handbook/sales/public-sector.md @james_harrison
/content/handbook/sales/qbrs/ @ashahrazad1 @azaglul @shannonthompson @monicaj
/content/handbook/sales/qualification-questions.md @chrisweber44
/content/handbook/sales/readmes/ @chrisweber44
/content/handbook/sales/revenue-programs.md @dsteer
/content/handbook/sales/sales-google-groups/ @monicaj @shannonthompson
/content/handbook/sales/sales-meetings.md @monicaj @chrisweber44
/content/handbook/sales/sales-operating-procedures/ @chrisweber44
/content/handbook/sales/sales-renewal-process.md @james_harrison
/content/handbook/sales/sales-term-glossary/ @james_harrison
/content/handbook/sales/selling-professional-services.md @james_harrison @dsakamoto
/content/handbook/sales/shadow-program.md @emelier @chrisweber44
/content/handbook/sales/sop.md @jrabbits
/content/handbook/sales/tam.md @jakebielecki
/content/handbook/sales/territories/ @james_harrison @lschoenfeld
/content/handbook/sales/territories/latam.md @disastor @ricardoamarilla
/content/handbook/sales/territory-planning.md @chrisweber44 @emelier
/content/handbook/sales/training/ @ashahrazad1 @jblevins608
/content/handbook/sales/training/field-certification/ @kshirazi @ashahrazad1 @pdaliparthi @emelier
/content/handbook/sales/training/field-functional-competencies.md @chrisweber44 @kshirazi
/content/handbook/sales/training/sales-enablement-sessions/ @ashahrazad1 @jblevins608
/content/handbook/sales/training/sko/ @kdemarest @kagarrett
/content/handbook/sales/understand-customer-gitlab-use.md @jakebielecki
/content/handbook/sales/training/customer-success-skills-exchange.md @chrisweber44 @pdaliparthi
/content/handbook/sales/field-communications/gitlab-highspot/ @monicaj
/content/job-families/sales/ @dhong @jbrennan1 @jakebielecki @james_harrison @jdbeaumont @cfarris

^[security]
/content/job-families/security/ @joshlemos @jlongo_gitlab
/content/handbook/security/ @joshlemos @jlongo_gitlab @plafoucriere
/content/handbook/security/architecture/ @plafoucriere
/content/handbook/security/security-operations/ @jfuentes2
/content/handbook/security/security-assurance/ @jburrows001 @tdilbeck @corey-oas @jlongo_gitlab @lcoleman
/content/handbook/security/threat-management/ @smanzuik @estrike
/content/handbook/security/threat-management/red-team/ @cmoberly
/content/handbook/security/security-engineering/ @jritchey
/content/handbook/security/security-engineering/automation.md  @agroleau
/content/handbook/security/security-engineering/infrastructure-security.md @joe-dub
/content/handbook/security/security-engineering/security-logging/ @joe-dub

^[solutions-architects]
/content/handbook/customer-success/solutions-architects/ @sa-leaders
/content/handbook/customer-success/solutions-architects/sa-practices/value-stream-discovery/ @simon_mansfield @reshmikrishna @jfullam

[support]
/content/handbook/support/ @gitlab-com/support @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/how-to-set-up-shift-coverage-for-an-event.md @gitlab-com/support/managers @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/personal_data_access_account_deletion.md @jcolyer @izzyfee @lyle @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/account_deletion_access_request_workflows.md @jcolyer @izzyfee @lyle @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/data_processing_addendums.md @lyle @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/information-request.md @lasayers @dfrhodes @emccrann @smccreesh @BronwynBarnett @rachelpack @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/log_requests.md @gitlab-com/support/managers @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/pii_removal_requests.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/dmca.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/reinstating-blocked-accounts.md @gitlab-com/support/managers @gitlab-com/gl-security/security-operations/trust-and-safety @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/working_with_security.md @gitlab-com/support/managers @gitlab-com/gl-security @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/unbabel_translation_in_zendesk.md @gitlab-com/support/managers @gitlab-com/support/support-ops @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/setting_ticket_priority.md @gitlab-com/support/managers @gitlab-com/support/support-ops @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/zendesk-instances.md @gitlab-com/support/managers @gitlab-com/support/support-ops @gitlab-com/egroup @gitlab-com/handbook
/content/handbook/support/workflows/team/performance_review.md @joergheilig @lbot @lyle @shaunmccann @vparsons @gitlab-com/egroup @gitlab-com/handbook

[support-readiness]
/content/handbook/support/readiness/ @lyle @jcolyer @gitlab-com/egroup @gitlab-com/handbook

^[teamops]
/content/teamops/ @nshah19
/content/handbook/teamops/ @nshah19 @bbehr
/content/handbook/teamops/partners.md @jmoverley @nshah19

^[Technical-Writing]
/content/handbook/product/ux/technical-writing/ @susantacker @dianalogan @kpaizee @david
/content/handbook/documentation.md @susantacker @dianalogan @kpaizee @david

^[total-rewards]
/content/handbook/total-rewards/ @shristikauffman @mwilkins

[values]
/content/handbook/values/ @sytses @gitlab-com/ceo-chief-of-staff-team @gitlab-com/egroup @gitlab-com/handbook

^[working-groups]
/content/handbook/company/working-groups/ @gitlab-com/egroup
/content/handbook/company/working-groups/clickhouse-datastore.md @clefelhocz1 @sgoldstein @nhxnguyen @nicolewilliams
/content/handbook/company/working-groups/cross-functional-prioritization.md @clefelhocz1 @vkarnes @meks @justinfarris
/content/handbook/company/working-groups/demo-test-data.md @grantyoung @poffey21 @vincywilson @meks
/content/handbook/company/working-groups/api-vision.md @g.hickman @arturoherrero @timzallmann
/content/handbook/company/working-groups/disaster-recovery.md  @mjwood @andrashorvath
/content/handbook/company/working-groups/frontend-observability.md @timzallmann @samdbeckham
/content/handbook/company/working-groups/multi-large.md @cdu1 @mbursi
/content/handbook/company/working-groups/software-supply-chain-security.md @sam.white @pcalder
/content/handbook/company/working-groups/ai-integration.md @hbenson @timzallmann @tmccaslin @wayne
