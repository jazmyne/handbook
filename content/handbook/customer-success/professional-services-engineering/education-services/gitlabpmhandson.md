---
title: "GitLab Project Management Hands-On Guide"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab Project Management course."
---

# GitLab Project Management Hands-On Guide


## GitLab Project Management labs

- [Lab 1: Access the GitLab training environment](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab1)
- [Lab 2: Create an organizational structure in GitLab](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab2)
- [Lab 3: Use GitLab planning tools](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab3)
- [Lab 4: Create issues](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab4)
- [Lab 5: Organize and manage issues](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab5)
- [Lab 6: Use a merge request to review and merge code](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab6)
- [Lab 7: Create and customize issue boards](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab7)
- [Lab 8: Create and manage a Kanban board](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab8)
- [Lab 9: Create and manage a Scrum board](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab9)
- [Lab 10: Create and manage a Waterfall board](/handbook/customer-success/professional-services-engineering/education-services/gitlabpmhandsonlab10)

## Quick links

Here are some quick links that may be useful when reviewing this Hands On Guide.

- [GitLab Project Management course description](/services/education/pm/)
- [GitLab Project Management Specialist certification details](/services/education/gitlab-project-management-associate/)

## Suggestions?

If you'd like to suggest changes to the *GitLab Project Management Hands-on Guide*, please submit them via merge request.
