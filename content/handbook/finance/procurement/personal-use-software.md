---
title: Individual Use Software
---

## Individual Use Software Overview

- Here we are defining individual use software as specific software that you need to perform your job function, outside of the existing software and platforms your team already owns/has access to. This spend should not exceed $5,000 annually. Should the cost of your software exceed this threshold, or if multiple people on your team need access, please follow the process outlined in the [New Software Procurement](/handbook/finance/procurement/new-software/) handbook page.
- Please note that **all software purchases charged to either personal or corporate credit cards will be DENIED for reimbursement.** You must follow the process below to cover your software purchases, which will be paid via Coupa virtual card once all necessary approvals are obtained and documented.

## I Need Individual Use Software- Where Do I Start?

- If you need a individual use software subscription, please first check the list of [Tech Stack applications](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml).
    - If your vendor is listed in the tech stack, please submit an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) using the Individual Bulk Access Request template to gain access.
- If GitLab does not have any existing agreements with your preferred vendor, please consider whether one of our existing vendors may be able to service your needs adequately. It is always preferable to add users to existing agreements that have already been screened and approved where possible, as this decreases GitLab’s risk and grants you access to the tools you need sooner.

## How Do I Submit a Request for New Individual Use Software?

- If you cannot find an existing software vendor that provides the service you need, please submit a request through this [Google form](https://docs.google.com/forms/d/e/1FAIpQLSfWp2lnSKp0gTKBfZxol-YEkTst4gtOYpRJQvnVvDzhcGuxwg/viewform).
- Procurement will open a GitLab Issue based on your responses and will go through the process of obtaining the appropriate approvals, which will include your manager, Security, and Legal. Once the Issue is opened, please attach any relevant contracts, backups, or documents. Reviews may take several weeks (3 or more), depending on the level of risk involved with your software of choice and the depth of review needed.
- Once all relevant parties have approved your software request in the GitLab Issue, Procurement will issue you a virtual credit card number for one-time use on the approved software license at the approved price. This information will come via email.
- After you have purchased your software with the virtual credit card, please forward a copy of your receipt to ap@gitlab.com as confirmation if there are no other contracts or backup documents.
- If you have any questions, please reach out to the Procurement team through the #procurement Slack channel.

## Pre-Approved Individual-Use Software Requests

- GitLab's Security Risk team evaluates software requests upon submission to determine the level of risk that may be associated with it's usage. Commonly requested software that has been reviewed and approved by Security Risk is documented in the [internal handbook](https://internal.gitlab.com/handbook/finance/procurement/individual-use-software/) and is subject to the listed pre-approval windows listed. See the [Third Party Risk Management Handbook Page](/handbook/security/security-assurance/security-risk/third-party-risk-management/) for more information on these procedures. Access requests associated to pre-approved software can bypass Security review.
- To get access for pre-approved software, please see the internal handbook for instructions on submitting the request to obtain necessary manager and budget approval.


