---
title: "Engineering Career Framework"
description: "These are the expected competencies of Engineering team members at GitLab by Job Title."
---

{{% include "includes/engineering/career-matrix-nav.md" %}}

These are the expected competencies of team members at GitLab by Job Title.

1. [Intern](/handbook/engineering/career-development/matrix/intern/)
1. [Associate](/handbook/engineering/career-development/matrix/associate/)
1. [Intermediate](/handbook/engineering/career-development/matrix/intermediate/)
1. [Senior](/handbook/engineering/career-development/matrix/senior/)
1. [Staff](/handbook/engineering/career-development/matrix/staff/)
1. [Principal](/handbook/engineering/career-development/matrix/principal/)


## Engineering Departments


|   | Sub Departments Career Frameworks                                                                                                                                                                                                              |
| ---- |------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **[Development](/handbook/engineering/career-development/matrix/development/)** | [Dev](/handbook/engineering/career-development/matrix/development/dev/), [Core Platform](/handbook/engineering/career-development/matrix/infrastructure/core-platform/), Growth, Ops, Package & Release, Secure & Govern, Verify |
| **[Incubation Engineering](/handbook/engineering/career-development/matrix/development/incubation/)** | Incubation Engineering |
| **[Infrastructure](/handbook/engineering/career-development/matrix/infrastructure/)** | Infrastructure, Delivery & Scalability, Reliability                                                                                                                                                                                            |
| **[Quality](/handbook/engineering/career-development/matrix/quality/)** | Dev, Ops, Secure, Core Platform, Engineering Productivity, Growth, Fulfillment, Govern                                                                                                                                                            |
| **[Support](/handbook/engineering/career-development/matrix/support/)** | [Support](/handbook/engineering/career-development/matrix/support/)                                                                                                                                                                |                                                                                                                                                                                     |
