---
title: "Development Department Career Framework: Staff"
---

## Development Department Competencies: Staff

{{% include "includes/engineering/dev-career-matrix-nav.md" %}}

**Staff at GitLab are expected to exhibit the following competencies:**

- [Staff Leadership Competencies](#staff-leadership-competencies)
- [Staff Technical Competencies](#staff-technical-competencies)
- [Staff Values Alignment](#staff-values-alignment)

---

### Staff Leadership Competencies

{{% include "includes/engineering/staff-leadership-competency.md" %}}
{{% include "includes/engineering/development-staff-leadership-competency.md" %}}
  
### Staff Technical Competencies

{{% include "includes/engineering/staff-technical-competency.md" %}}
{{% include "includes/engineering/development-staff-technical-competency.md" %}}

### Staff Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-staff-values-competency.md" %}}
