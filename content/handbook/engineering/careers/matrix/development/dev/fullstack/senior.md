---
title: "Dev Career Framework: Senior Fullstack Engineer"
---

## Dev Senior Fullstack Engineer
  
{{% include "includes/engineering/dev-fullstack-career-matrix-nav.md" %}}

**Dev Senior Fullstack Engineers at GitLab are expected to exhibit the following competencies:**

- [Leadership Competencies](#leadership-competencies)
- [Technical Competencies](#technical-competencies)
- [Values Alignment](#values-alignment)

---

### Leadership Competencies

{{% include "includes/engineering/senior-leadership-competency.md" %}}
{{% include "includes/engineering/development-senior-leadership-competency.md" %}}

### Technical Competencies

{{% include "includes/engineering/senior-technical-competency.md" %}}
{{% include "includes/engineering/development-senior-technical-competency.md" %}}

###  Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-senior-values-competency.md" %}}
